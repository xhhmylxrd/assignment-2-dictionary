%include "lib.inc"
%include "words.inc"
%define MAX_STR_LEN 256
%define NEXT_AND_NUL_TERM 9
%define WRITE_CODE 1
extern find_word
global _start

section .data

section .bss
buffer: times MAX_STR_LEN db 0

section .rodata
overflow_message: db "String must be smaller than 256 symbols", 0
not_found_message db "There's no node with this key in the dictionaty", 0

section .text


print_error:
	push rdi	
	call string_length ;в rax запишется длина строки
	pop rsi
	mov rdx, rax
	mov rdi, 2
	mov rax, WRITE_CODE
	syscall
	ret


_start:

	mov rdi, buffer
	mov rsi, MAX_STR_LEN
	
	call read_word

	cmp rax, 0
	je .overflow

	mov rdi, rax
	mov rsi, next_node
	
	call find_word

	cmp rax, 0
	je .not_found
	
	add rax, NEXT_AND_NUL_TERM
	add rax, rdx

	mov rdi, rax
	call print_string
	call exit

.overflow:
	mov rdi, overflow_message
	call print_error
	call exit
.not_found:
	mov rdi, not_found_message
	call print_error
	call exit
	
