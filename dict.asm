section .text
%include "lib.inc"
global find_word

%define NEXT_POINTER_LEN 8

; rdi - string pointer
; rsi linked list node pointer

find_word:
.loop:
	cmp rsi, 0
	je .fail	

	push rsi
	push rdi
	
	add rsi, NEXT_POINTER_LEN
	call string_equals

	pop rdi
	pop rsi

	cmp rax, 0
	jne .success
	mov rsi, [rsi]
	jmp .loop
.success:
	mov rax, rsi
	ret
.fail:
	xor rax, rax
	ret
