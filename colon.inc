%define next_node 0

%macro colon 2
%2:
	dq next_node
	
	%define next_node %2

	db %1, 0
%endmacro


