ASM=nasm
FORMAT=elf64
LD=ld

build: main clean

main: main.o dict.o lib.o
	$(LD) -o $@ $^

main.o: main.asm colon.inc lib.inc words.inc
	$(ASM) -f $(FORMAT) -o $@ $<

%.o: %.asm
	$(ASM) -f $(FORMAT) -o $@ $^

.PHONY: clean build

clean:
	rm *.o

